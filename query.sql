-- Câu lệnh truy vấn lấy danh sách sinh viên có tên khoa là "Công nghệ thông tin"
SELECT `sinhvien`.`MaSV`, `sinhvien`.`HoSV`, `sinhvien`.`TenSV`, `sinhvien`.`GioiTinh`, `sinhvien`.`NgaySinh`, `sinhvien`.`NoiSinh`, `sinhvien`.`DiaChi`, `sinhvien`.`MaKH`,`sinhvien`.`HocBong`
FROM `sinhvien` 
LEFT JOIN `dmkhoa` ON `sinhvien`.`MaKH` = `dmkhoa`.`MaKH`
WHERE `dmkhoa`.`TenKhoa` LIKE "Công nghệ thông tin"

-- Cách khác 
SELECT * 
FROM sinhvien
WHERE MaKH LIKE (
    SELECT `dmkhoa`.`MaKH` 
    FROM `dmkhoa` 
    WHERE `dmkhoa`.`TenKhoa` LIKE "Công nghệ thông tin")