-- Tạo database tên QLSV 
CREATE DATABASE QLSV

-- Tạo bảng DMKHOA
CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6),
  `TenKhoa` varchar(30)
)
-- Thêm khóa chính là MaKH
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);


-- Tạo bảng SINHVIEN
CREATE TABLE `sinhvien` (
  `MaSV` varchar(6),
  `HoSV` varchar(30),
  `TenSV` varchar(15),
  `GioiTinh` char(1),
  `NgaySinh` datetime,
  `NoiSinh` varchar(50),
  `DiaChi` varchar(50),
  `MaKH` varchar(6),
  `HocBong` int(11)
)
-- Thêm khóa chính bảng SinhVien là MaSV
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`);


